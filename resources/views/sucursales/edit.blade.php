@extends('layouts.app')
@section('title','Sucursales Create')

@section('content')
        <H1>EDITAR SUCURSALES</H1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
            </div>
        @endif    
        
        <form class="form-group" method="POST" action="/sucursales/{{$sucursal->slug}}"   enctype="multipart/form-data" accept-charset="UTF-8">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label for="">Nombre Sucursal</label>
            <input type="text" value="{{$sucursal->sucursal}}" name="sucursal" class="form-control">
            </div>     

            <button type="submit" class="btn btn-primary">Actualizar</button>
        </form>
        
    
@endsection