@extends('layouts.app')
@section('title','Sucursales List')

@section('content')
        <H1>LISTA SUCURSALES</H1>
        <a href="/sucursales/create" class="btn btn-primary">Agregar</a>

        
        <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">No</th>
                <th scope="col">Sucursal</th>
                <th scope="col">Actualizar</th>
                <th scope="col">Eliminar</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sucursales as $sucursal)
                <tr>
                    <th scope="row">{{$sucursal->id}}</th>
                    <td>{{$sucursal->sucursal}}</td>
                    <td>
                    <a href="/sucursales/{{$sucursal->slug}}/edit" class="btn btn-warning">Actualizar</a>
                    </td>
                    <td>
                        
                        <form class="form-group" method="POST" action="/sucursales/{{$sucursal->slug}}"   enctype="multipart/form-data" accept-charset="UTF-8">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                               
                
                            <button type="submit" class="btn btn-danger">Elimiar</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
                    
    
@endsection