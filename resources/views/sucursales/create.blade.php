@extends('layouts.app')
@section('title','Sucursales Create')

@section('content')
        <H1>CREAR SUCURSALES</H1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
            </div>
        @endif                          
        
        <form class="form-group" method="POST" action="/sucursales"  enctype="multipart/form-data" accept-charset="UTF-8">
           @include('sucursales.form')           

            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
        
    
@endsection