<?php

namespace TiendaComics\Http\Controllers;

use TiendaComics\Sucursal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use TiendaComics\Http\Requests\StoreSucursalRequest;

class SucursalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        #return 'AQUI VAN LAS SUCURSALES';
        $sucursales = Sucursal::all();
        return view('sucursales.index',compact('sucursales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sucursales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSucursalRequest $request)
    {
        
        $sucursal = new Sucursal();
        $slug = str_replace(' ', '', $request->input('sucursal'));
        $sucursal->sucursal = $request->input('sucursal');
        $sucursal->slug = $slug;
        $sucursal->save();
        return redirect()->route('sucursales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $sucursal = Sucursal::where('slug','=',$slug)->firstOrFail();
        #return $sucursal;
        return view('sucursales.edit',compact('sucursal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSucursalRequest $request, $slug)
    {
        
        $sucursal = Sucursal::where('slug','=',$slug)->firstOrFail();
        $slug = str_replace(' ', '', $request->input('sucursal'));
        $sucursal->sucursal = $request->input('sucursal');
        $sucursal->slug = $slug;
        $sucursal->save();
        return redirect()->route('sucursales.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $sucursal = Sucursal::where('slug','=',$slug)->firstOrFail();
        $sucursal->delete();
        return redirect()->route('sucursales.index');
    }
}
