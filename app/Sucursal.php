<?php

namespace TiendaComics;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
